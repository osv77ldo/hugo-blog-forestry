# Readme

This site was generated whit [Hugo] and is being hosted on gitlab pages and netlify
- Hugo v0.79.1
- forestry.io
- netlify.app

## GitLab CI

This project's static Pages are built by gitlab ci, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project (Be sure to do this **recursive!**)
1. Install Hugo 0.55 or higger (extended version for scss support)
1. Preview your project: `hugo server`
1. Add content

### Development preview

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/`.

### Netlify Integration

If you want to deploy to netlify, make sure to configure your netlify project and creating two variables on gitlab secrets
NETLIFY_AUTH_TOKEN
NETLIFY_NETLIFY_SITE_ID
